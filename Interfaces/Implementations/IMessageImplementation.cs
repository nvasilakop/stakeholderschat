﻿using DataTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Implementations
{
    public interface IMessageImplementation
    {
        List<Message> GetMessages();
    }
}
