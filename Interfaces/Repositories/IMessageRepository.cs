﻿using DataTypes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Interfaces.Repositories
{
    public interface IMessageRepository
    {
        List<Message> GetMessages();
    }
}
