﻿using DataTypes;
using Interfaces.Repositories;
using PetaPoco.NetCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Worker;


namespace Repositories
{

        public class MessageRepository : IMessageRepository
        {
            private readonly PetaPoco.NetCore.Database db;

            public MessageRepository(DbProviders.SignalRDb conn)
            {
                db = new PetaPoco.NetCore.Database(conn());
            }

            public List<Message> GetMessages()
            {
             return db.Query<Message>(Sql.Builder.Append("SELECT * FROM dbo.Message")).ToList();

            }
        }
    
}
