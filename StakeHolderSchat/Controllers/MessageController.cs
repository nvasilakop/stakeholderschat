﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Interfaces.Implementations;
using Microsoft.AspNetCore.Mvc;

namespace StakeHolderSchat.Controllers
{
    public class MessageController : Controller
    {
        private readonly IMessageImplementation impl;
        public MessageController(IMessageImplementation _impl)
        {
            impl = _impl;
        }
        // GET: Message
        public ActionResult Index()
        {

            return View(impl.GetMessages());
        }
    }
}