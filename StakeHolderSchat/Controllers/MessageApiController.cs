﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DataTypes;
using Interfaces.Implementations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace StakeHolderSchat.Controllers
{
    [Produces("application/json")]
    [Route("api/MessageApi")]
    public class MessageApiController : Controller
    {
        private readonly IMessageImplementation impl;
        public MessageApiController(IMessageImplementation _impl)
        {
            impl = _impl;
        }

        [HttpGet("getMessages")]
        public List<Message> Index()
        {

            return impl.GetMessages();
        }
    }
}