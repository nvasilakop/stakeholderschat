﻿using Repositories;
using Interfaces;
using System;
using Interfaces.Implementations;
using DataTypes;
using System.Collections.Generic;
using Interfaces.Repositories;

namespace Implementations
{
    public class MessageImplementation : IMessageImplementation
    {
        private readonly IMessageRepository _repo;

        public MessageImplementation(IMessageRepository repo)
        {
            _repo = repo;
        }
        public List<Message> GetMessages()
        {
            return _repo.GetMessages();
        }
    }
}
