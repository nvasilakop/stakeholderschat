﻿using System;

namespace DataTypes
{
    public class Message
    {
        public Guid Id { get; set; }
        public string MessageContent { get; set; }
    }
}
